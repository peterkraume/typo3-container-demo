<?php
defined('TYPO3_MODE') or die();

/**
 * add an entry in the static template list found in sys_templates
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'demo_sitepackage',
    'Configuration/TypoScript',
    'Demo Sitepackage for EXT:container'
);
