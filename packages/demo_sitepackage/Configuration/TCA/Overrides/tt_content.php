<?php

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'demo-2cols-container', // CType
            '2 Column Container', // label
            'Some Description of the Container', // description
            [
                // row 1
                [
                    // column 1
                    ['name' => 'left side', 'colPos' => 201, 'allowed' => ['CType' => 'header, textmedia']], // allowed requires EXT:content_defender
                    // column 2
                    ['name' => 'right side', 'colPos' => 202]
                ]
            ] // grid configuration
        )
    )
    // override default configurations
    ->setIcon('EXT:container/Resources/Public/Icons/container-2col.svg')
    //->setSaveAndCloseInNewContentElementWizard(false)
    //->setRegisterInNewContentElementWizard(false)
    // see https://github.com/b13/container#methods-of-the-containerconfiguration-object for more configuraton settings
);

// override default settings
$GLOBALS['TCA']['tt_content']['types']['demo-2cols-container']['showitem'] = 'sys_language_uid,CType,header,tx_container_parent,colPos';