<?php
defined('TYPO3_MODE') or die();

/**
 * add an entry in the include static Page TSconfig list in page properties
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'demo_sitepackage',
    'Configuration/TSconfig/Page.tsconfig',
    'Global page TSconfig for EXT:container demo'
);
