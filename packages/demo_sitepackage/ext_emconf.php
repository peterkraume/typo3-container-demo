<?php

$EM_CONF['demo_sitepackage'] = [
    'title' => 'Demo Sitepackage for EXT:container',
    'description' => 'Demo Sitepackage for EXT:container',
    'category' => 'Frontend',
    'author' => 'Peter Kraume',
    'author_email' => 'peter.kraume@gmx.de',
    'author_company' => 'Kraume Web Services',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '1.0.0',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '10.4.0-10.4.99',
                ],
            'conflicts' =>
                [
                ],
            'suggests' =>
                [
                ],
        ],
];
