# Demo of TYPO3 extension EXT:container

## Installation:

- To use this TYPO3 demo installation you need [DDEV-local](https://www.ddev.com/ddev-local/). It's free!
- Clone this repository.
- Run `ddev start && ddev composer install`.
- Import the database dump: `ddev import-db --src=.assets/dump.sql.gz`
- Run `ddev launch typo3` or open https://container-demo.ddev.site/typo3/ in your browser
- TYPO3 Backend Credentials:
    - User: admin
    - Password: password

## Getting started in your own project

- Install EXT:container (`composer req b13/container` or via TER)
- Recommended: install EXT:content_defender (`composer req ichhabrecht/content-defender` or via TER)
- Add [Configuration/TCA/Overrides/tt_content.php](https://gitlab.com/peterkraume/typo3-container-demo/-/blob/master/packages/demo_sitepackage/Configuration/TCA/Overrides/tt_content.php) to your theme extension or sitepackage and configure the desired container for the TYPO3 backend
- Add [TypoScript](https://gitlab.com/peterkraume/typo3-container-demo/-/blob/master/packages/demo_sitepackage/Configuration/TypoScript/Setup/Content.typoscript) to define the rendering of the container in the frontend
- Add a [Fluid template](https://gitlab.com/peterkraume/typo3-container-demo/-/blob/master/packages/demo_sitepackage/Resources/Private/Templates/Content/Templates/2Cols.html) for your container
- And you're done!

## Links

- EXT:container https://github.com/b13/container
- Example Extension https://github.com/b13/container-example
- Blog Posts by b13 (Attention: demo code might be outdated!)
    - https://b13.com/blog/flexible-containers-and-grids-for-typo3
    - https://b13.com/blog/flexible-containers-and-grids-for-typo3-part-two
    - https://b13.com/blog/flexible-containers-and-grids-for-typo3-part-three
- EXT:content_defender https://github.com/IchHabRecht/content_defender
